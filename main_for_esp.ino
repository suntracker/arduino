#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 5); // RX, TX

char tmode = '0';
const char* ssid = "Redmi";
const char* password = "11111111";
HTTPClient http;


void setup() {
  Serial.begin(4800);
  mySerial.begin(4800);
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());
  Serial.println("1...");
  Serial.println("2...");
  Serial.println("Serial started");
}

void loop(){
  int data = -1;
  if (tmode == '1') {
    mySerial.print('?');
    delay(20);
    data = mySerial.read();
  }
  String arduino = "http://192.168.43.64/code/SunTracker2/arduino.php?data=";
  http.begin(arduino + String(data));
  int httpCode = http.GET(); //Send the request
  Serial.println(httpCode);
  if (httpCode > 0) { //Check the returning code
    String payload = http.getString(); //Get the request response payload
    Serial.println(payload); //Print the response payload
    if (payload != "n") {
      if (payload[0] != 'n') {
        tmode = payload[0];
        mySerial.print(tmode);
      }
    }
  } else {
    Serial.println("mistacke");
  }
  delay(1000);
}
