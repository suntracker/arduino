const int pinPhoto1 = A0;
const int pinPhoto2 = A1;
const int pinD1 = 2;
const int pinD2 = 3;
int PhVal1 = 0;
int PhVal2 = 0;
String S; 

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600);
 pinMode( pinPhoto1, INPUT ); 
 pinMode( pinPhoto2, INPUT ); 
 pinMode( pinD1, OUTPUT );
 pinMode( pinD2, OUTPUT ); 
 digitalWrite(pinD1,LOW);
 digitalWrite(pinD2,LOW);
  

}

void loop() {
  // put your main code here, to run repeatedly:
  PhVal1 = analogRead( pinPhoto1 );
  PhVal2 = analogRead( pinPhoto2 );
  if ((PhVal1-PhVal2) > 100) 
   {
    digitalWrite(pinD1,HIGH);
    digitalWrite(pinD2,LOW);
   }
  
  if (abs(PhVal1-PhVal2) <= 100)
   {
    digitalWrite(pinD1,LOW);
    digitalWrite(pinD2,LOW);  
   }
 
  if ((PhVal2-PhVal1) > 100) 
   {
    digitalWrite(pinD1,LOW);
    digitalWrite(pinD2,HIGH);
   } 
  S = "Val_1 = " + String(PhVal1) + "    Val_2 = " + String(PhVal2);
  Serial.println(String(S));
  delay(500); 

}