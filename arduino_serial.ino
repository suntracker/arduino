#include <SoftwareSerial.h>

SoftwareSerial mySerial(6, 5); // RX, TX

void setup() {
  Serial.begin(4800);
  mySerial.begin(4800);
  Serial.println("1..");
  Serial.println("2..");
  Serial.println("start");
}

void loop() {
  if (mySerial.available()){
    int a = mySerial.read();
    char c = char(a);
    Serial.println(c);
    if (c == 'b') {
      delay(1);
      mySerial.print('u');
    }
  }
}